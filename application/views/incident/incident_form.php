		<section>
			<div id ="container">
				<div class = "row">
					<div class ="col-md-3" id = "pag_initial_menu">
						<div id='background'>
						<?php $avatar = $avatar_array[0]->avatar; 
						$avatar_base_url = base_url()."static/avatar/";
						$aux = $avatar_base_url.$avatar;
						?>
						<img src="<?php echo $aux;?>" target="_blank" class="img-circle" id = "avatar">
						<p id="user_name"><?php echo $this->session->userdata('nome_usuario');?></p>
						</div>
						<div>
						<ul class ="list_nav">
							<li class="nav_links">
							<a href ="<?php echo base_url();?>dashboard">
							<p class = "text_nav">Meus incidentes</p></a><div class = "incident_cont"><?php echo $count_incident[0]->incidents;?></div></li>
							<li class="nav_links">
							<a href ="<?php echo base_url();?>my_data"> 
							<p class = "text_nav">Meus dados</p></a></li>
							<li class="nav_links">
							<a href ="<?php echo base_url();?>login/sair">
							<p class = "text_nav">Sair</p></a></li>
						</ul>
						<i id = "nav_icon1" class="glyphicon glyphicon-off"></i>
						<i id = "nav_icon2" class="glyphicon glyphicon-user"></i>
						<i id = "nav_icon3" class="glyphicon glyphicon-th-list"></i>
						</div>
						</div>
				</div>
				<div>
					<h1 class = "incident_text1">Meus Incidentes Abertos</h1>
					<a id ="incident_btn1" class="btn btn-primary" href="<?php echo base_url();?>incident/criar">+ Abrir novo Incidente</a>
					<div class ="incident_form">
						<div style="position: relative;left: 23;top: 37;">
						<h2>Criar um novo incidente</h2>
						</div>
							<form action="" onsubmit="return onSubmit()" enctype="multipart/form-data" method ="POST">
								<p class = "opt_text_form1">Indisponibilidade</p>
								<select class ="form-control" id ="incident_radio" name ="incident_radio">
									<option value="0" disabled selected>Selecione o tipo de Incidente</option>
									<option value="1">duvida</option>
									<option value ="2">sugestão</option>
									<option value ="3">reclamação</option>
								</select>
							<p class = "opt_text_form2">Assunto</p>
							<input type="text" class = "subject_input" name = "subject_input" placeholder = "Digite o assunto que resume seu incidente"></input>
							<p class = "opt_text_form3">Descrição</p>
							<textarea type="text" class = "description_input" name = "description_input" placeholder = "Digite o passo a passo executado que gerou o problema, se possível envie uma imagem da tela com a mensagem de erro apresentada"></textarea>
							<p class = "opt_text_form4">Anexo</p>
							<input class = "button_upload" type="file" name = "upload_file">
							<button id ="btn_submit_incident" type="submit" class="btn btn-primary">Notificar Incidente</button>
							</form>
						</div>
				</div>
			</div>
		</section>