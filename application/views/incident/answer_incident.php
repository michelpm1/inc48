		<section>
			<div id ="container">
				<div class = "row">
					<div class ="col-md-3" id = "pag_initial_menu">
						<div id='background'>
						<?php $avatar = $avatar_array[0]->avatar; 
						$avatar_base_url = base_url()."static/avatar/";
						$aux = $avatar_base_url.$avatar;
						?>
						<img src="<?php echo $aux;?>" target="_blank" class="img-circle" id = "avatar">
						<p id="user_name"><?php echo $this->session->userdata('nome_usuario');?></p>
						</div>
						<div>
						<ul class ="list_nav">
							<li class="nav_links">
							<a href ="<?php echo base_url();?>dashboard_admin">
							<p class = "text_nav">Incidentes</p></a><div class = "incident_cont"><?php echo $count_incident[0]->incidents;?></div></li>
							<li class="nav_links">
							<a href ="<?php echo base_url();?>my_data_admin"> 
							<p class = "text_nav">Meus dados</p></a></li>
							<li class="nav_links">
							<a href ="<?php echo base_url();?>login/sair">
							<p class = "text_nav">Sair</p></a></li>
						</ul>
						<i id = "nav_icon1" class="glyphicon glyphicon-off"></i>
						<i id = "nav_icon2" class="glyphicon glyphicon-user"></i>
						<i id = "nav_icon3" class="glyphicon glyphicon-th-list"></i>
						</div>
						</div>
				</div>
				<div>
					<h1 class = "incident_text1_detail_page">Atendendo o Incidente</h1>
					<div class ="incident_answer_div">
						<div style="position: relative;left: 23;top: 37;">
						<h2></h2>
						</div>
						<h2 id="detail_titul"><?php echo $incident_show_by_id[0]->titulo;?></h2>
						<p id ="answer_descrip"><?php echo $incident_show_by_id[0]->descricao;?></p>
						<p id ="detail_anex">Anexo:  <?php echo $incident_show_by_id[0]->anexo;?></p>
						<form action="" onsubmit="return onSubmit()" method ="POST">
						<textarea type="text" class = "incident_answer" name = "answer_input"></textarea>
						<button id ="send_incident_answer" type="submit" class="btn btn-primary">Finalizar Atendimento</button>
						</form>
						
						</div>
				</div>
			</div>
		</section>