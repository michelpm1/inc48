		<section>
			<div id ="container">
				<div class = "row">
					<div class ="col-md-3" id = "pag_initial_menu">
						<div id='background'>
						<?php $avatar = $avatar_array[0]->avatar; 
						$avatar_base_url = base_url()."static/avatar/";
						$aux = $avatar_base_url.$avatar;
						?>
						<img src="<?php echo $aux;?>" target="_blank" class="img-circle" id = "avatar">
						<p id="user_name"><?php echo $this->session->userdata('nome_usuario');?></p>
						</div>
						<div>
						<ul class ="list_nav">
							<li class="nav_links">
							<a href ="<?php echo base_url();?>dashboard_admin">
							<p class = "text_nav">Incidentes</p></a><div class = "incident_cont"><?php echo $count_incident[0]->incidents;?></div></li>
							<li class="nav_links">
							<a href ="<?php echo base_url();?>my_data_admin"> 
							<p class = "text_nav">Meus dados</p></a></li>
							<li class="nav_links">
							<a href ="<?php echo base_url();?>login/sair">
							<p class = "text_nav">Sair</p></a></li>
						</ul>
						<i id = "nav_icon1" class="glyphicon glyphicon-off"></i>
						<i id = "nav_icon2" class="glyphicon glyphicon-user"></i>
						<i id = "nav_icon3" class="glyphicon glyphicon-th-list"></i>
						</div>
						</div>
				</div>
				<div>
					<h1 class = "incident_text1_detail_page">Detalhes do Incidente</h1>
					<div class ="incident_detail">
						<div style="position: relative;left: 23;top: 37;">
						<h2></h2>
						</div>
						<h2 id="detail_titul"><?php echo $incident_show_by_id[0]->titulo;?></h2>
						<p id ="detail_descrip"><?php echo $incident_show_by_id[0]->descricao;?></p>
						<p id ="detail_type">
						<?php if ($incident_show_by_id[0]->tipo == 1)
						{
							echo "Tipo do incidente: Dúvida";
						}
						elseif ($incident_show_by_id[0]->tipo == 2)
						{
							echo "Tipo do incidente: Sugestão";
						}
						else{
							echo "Tipo do incidente: Reclamação";
						}?></p>
						<p id ="detail_anex">Anexo:  <?php echo $incident_show_by_id[0]->anexo;?></p>
						<p id ="detail_creation"><?php echo $incident_show_by_id[0]->criacao;?></p>
						<p id ="detail_owner">Por:	<?php echo $incident_own[0]->name;?></p>
						</div>
				</div>
			</div>
		</section>