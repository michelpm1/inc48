		<section data-urlbase = "<?php echo base_url();?>" id = "base_url">
			<div id ="container">
				<div class = "row">
					<div class ="col-md-3" id = "pag_initial_menu">
						<div id='background'>
						<?php $avatar = $avatar_array[0]->avatar; 
						$avatar_base_url = base_url()."static/avatar/";
						$aux = $avatar_base_url.$avatar;
						?>
						<img src="<?php echo $aux;?>" target="_blank" class="img-circle" id = "avatar">
						<p id="user_name"><?php echo $this->session->userdata('nome_usuario');?></p>
						</div>
						<div>
						<ul class ="list_nav">
							<li class="nav_links">
							<a href ="<?php echo base_url();?>dashboard_admin">
							<p class = "text_nav">Incidentes</p></a><div class = "incident_cont"><?php echo $count_incident[0]->incidents;?></div></li>
							<li class="nav_links">
							<a href ="<?php echo base_url();?>my_data_admin"> 
							<p class = "text_nav">Meus dados</p></a></li>
							<li class="nav_links">
							<a href ="<?php echo base_url();?>login/sair">
							<p class = "text_nav">Sair</p></a></li>
						</ul>
						<i id = "nav_icon1" class="glyphicon glyphicon-off"></i>
						<i id = "nav_icon2" class="glyphicon glyphicon-user"></i>
						<i id = "nav_icon3" class="glyphicon glyphicon-th-list"></i>
						</div>
						</div>
						<div class = "col-md-8">
							<h1 class = "incident_text">Incidentes em Aberto</h1>
							<!--<?php //if($this->session->flashdata('db_message')){?> sessao com mensagem de acidente criada com sucesso se tiver na sessao ele cria a div e mostra a mensagem
								<h3><?php //echo $this->session->flashdata('db_message'); ?></h3>
								<?php// } ?> -->
							<div class = "row"></div>
								<div class = "col-md-11" id="open_incidents">
									<div id = "open_incident1">
										<ul id = "incident_ul_open">
											<?php
												foreach($all_incident as $u)
												{ ?>
													<li id = "incident_list">
														<div class = "row">
															<div class = "col-md-10">
																<div id = "upper_incident_layout"><h3><?php echo $u->titulo;?></h3></div>
																<div id = "botton_incident_layout">
																	<a class = "btn btn-success" href="<?php echo base_url();?>detail_page/view_detail/<?php echo $u->id; ?>">Ver Detalhes</a>
																	<a class = "btn btn-primary" href="<?php echo base_url();?>answer_incident/reply_incident/<?php echo $u->id; ?>">Iniciar Atendimento</a>
																	<i id = "icon_flag_open_admin" class="glyphicon glyphicon-flag"></i>
																	<p id = "status_layout">
																		<?php if ($u->status == 0)
																		{
																			echo 'Aguardando atendimento';
																		} else 
																		{
																			echo 'Finalizado';
																		}?>
																	</p>
																</div>
															</div>
															<div class = "col-md-2" id="right_incident_layout"><p id ="text_day"><?php $aux = $u->criacao; $aux1 = substr($aux, 0, 2); echo $aux1;?></p><p id = "text_data"><?php $aux = $u->criacao; $aux1 = substr($aux, 2); echo $aux1;?></p></div>
														</div>
													</li>
												<?php } ?>
											</ul>
											<div class = 'row'>
												<div>
													<a class = "btn btn-primary" id ="number_open" data-value = "6">Ver mais incidentes</a>
												</div>
											</div>
									</div>
								</div >
							<div id="btn_div">
								
							</div>
					</div>
				</div>
				</div>
			</div>
		<script>
		$("#number_open").on("click", function(ev){
			base_url = $("#base_url").attr("data-urlbase");
			number = $("#number_open").attr("data-value");
			$.getJSON(base_url+"incident/ver_mais_all/"+number, function(data)
			{
				$.each(data, function(index, item){
					console.log (item.titulo);
						if (item.status == 0)
							{
								var aux = 'Aguardando atendimento';
							} else 
							{
								var aux = 'Finalizado';
							}
							var aux2 = item.criacao.substr(0,2);
							var text = '<li id = "incident_list">'+
														'<div class ="row">'+
															'<div class ="col-md-10">'+
															'<div id = "upper_incident_layout"><h3>'+item.titulo+'</h3></div>'+
															'<div id = "botton_incident_layout">'+
																'<a class = "btn btn-success" href="'+base_url+'detail_page/view_detail/'+item.id+ '"> Ver Detalhes</a>'+
																' <a class = "btn btn-primary" href="'+base_url+'answer_incident/reply_incident/'+item.id+ '">Iniciar Atendimento</a>'+
																' <i id = "icon_flag_open_admin" class="glyphicon glyphicon-flag"></i>'+
																'<p id = "status_layout">'+
																aux+
																'</p>'+
															'</div>'+
														'</div>'+
														'<div class = "col-md-2" id="right_incident_layout"><p id ="text_day">'+aux2+'</p><p id = "text_data"><?php $aux = $u->criacao; $aux1 = substr($aux, 2); echo $aux1;?></p></div>'+
													'</li>';
					$("#incident_ul_open").append(text);
				});
		$("#number_open").attr("data-value", parseInt(number)+6);
			});
			ev.preventDefault();
		});
		</script>
		</section>