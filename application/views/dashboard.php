		<section data-urlbase = "<?php echo base_url();?>" id = "base_url">
			<div id ="container">
				<div class = "row">
					<div class ="col-md-3" id = "pag_initial_menu">
						<div id='background'>
						<?php $avatar = $avatar_array[0]->avatar; 
						$avatar_base_url = base_url()."static/avatar/";
						$aux = $avatar_base_url.$avatar;
						?>
						<img src="<?php echo $aux;?>" target="_blank" class="img-circle" id = "avatar">
						<p id="user_name"><?php echo $this->session->userdata('nome_usuario');?></p>
						</div>
						<div>
						<ul class ="list_nav">
							<li class="nav_links">
							<a href ="<?php echo base_url();?>dashboard">
							<p class = "text_nav">Meus incidentes</p></a><div class = "incident_cont"><?php echo $count_incident[0]->incidents;?></div></li>
							<li class="nav_links">
							<a href ="<?php echo base_url();?>my_data"> 
							<p class = "text_nav">Meus dados</p></a></li>
							<li class="nav_links">
							<a href ="<?php echo base_url();?>login/sair">
							<p class = "text_nav">Sair</p></a></li>
						</ul>
						<i id = "nav_icon1" class="glyphicon glyphicon-off"></i>
						<i id = "nav_icon2" class="glyphicon glyphicon-user"></i>
						<i id = "nav_icon3" class="glyphicon glyphicon-th-list"></i>
						</div>
						</div>
						<div class = "col-md-8">
							<h1 class = "incident_text">Meus Incidentes</h1>
							<!--<?php //if($this->session->flashdata('db_message')){?> sessao com mensagem de acidente criada com sucesso se tiver na sessao ele cria a div e mostra a mensagem
								<h3><?php //echo $this->session->flashdata('db_message'); ?></h3>
								<?php// } ?> -->
								<a id ="incident_btn" class="btn btn-primary" href="<?php echo base_url();?>incident/criar">+ Abrir novo Incidente</a>
								<a id ="open_btn" class="btn btn-default" >Em Aberto</a>
								<a id ="closed_btn" class="btn btn-default" >Finalizados</a>
							<div class = "row"></div>
								<div class = "col-md-11" id="open_incidents">
									<div id = "open_incident1">
										<ul id = "incident_ul_open">
											<?php
												foreach($incident_show_open as $u)
												{ ?>
													<li id = "incident_list">
														<div class = "row">
															<div class = "col-md-10">
																<div id = "upper_incident_layout"><h3><?php echo $u->titulo;?></h3></div>
																<div id = "botton_incident_layout">
																	<a class = "btn btn-success" href="<?php echo base_url();?>detail_page/view_detail/<?php echo $u->id; ?>">Ver Detalhes</a>
																	<form id = "dashboard_form_layout" action="" onsubmit="return onSubmit()" method ="POST">
																		<button class = "btn btn-success" name="incident_id" value="<?php echo $u->id; ?>" type="submit" >Excluir</button>
																	</form>
																	<a class = "btn btn-success" href="<?php echo base_url();?>incident/editar/<?php echo $u->id; ?>">Editar</a>
																	<i id = "icon_flag_open" class="glyphicon glyphicon-flag"></i>
																	<p id = "status_layout">
																		<?php if ($u->status == 0)
																		{
																			echo 'Aguardando atendimento';
																		} else 
																		{
																			echo 'Finalizado';
																		}?>
																	</p>
																</div>
															</div>
															<div class = "col-md-2" id="right_incident_layout"><p id ="text_day"><?php $aux = $u->criacao; $aux1 = substr($aux, 0, 2); echo $aux1;?></p><p id = "text_data"><?php $aux = $u->criacao; $aux1 = substr($aux, 2); echo $aux1;?></p></div>
														</div>
													</li>
												<?php } ?>
											</ul>
											<div class = 'row'>
												<div>
													<a class = "btn btn-primary" id ="number_open" data-value = "6">Ver mais incidentes</a>
												</div>
											</div>
									</div>
								</div >
								<div class = "col-md-11" id="closed_incidents">
									<div id = "closed_incident1">
										<ul id = "incident_ul_closed">
											<?php
												foreach($incident_show_closed as $u){
												?>
													<li id = "incident_list">
														<div class = "row">
															<div class = "col-md-10">
															<div id = "upper_incident_layout"><h3><?php echo $u->titulo;?></h3></div>
															<div id = "botton_incident_layout">
																<a class = "btn btn-success" href="<?php echo base_url();?>detail_page/view_detail/<?php echo $u->id; ?>">Ver Detalhes</a>
																<form id = "dashboard_form_layout" action="" onsubmit="return onSubmit()" method ="POST">
																	<button class = "btn btn-success" name="incident_id" value="<?php echo $u->id; ?>" type="submit" >Excluir</button>
																</form>
																<a class = "btn btn-success" href="<?php echo base_url();?>incident/editar/<?php echo $u->id; ?>">Editar</a>
																<i id = "icon_flag_closed" class="glyphicon glyphicon-flag"></i>
																<p id = "status_layout">
																	<?php if ($u->status == 0)
																	{
																		echo 'Aguardando atendimento';
																	} else 
																	{
																		echo 'Finalizado';
																	}?>
																</p>
															</div>
														</div>
														<div class = "col-md-2" id="right_incident_layout"><p id ="text_day"><?php $aux = $u->criacao; $aux1 = substr($aux, 0, 2); echo $aux1;?></p><p id = "text_data"><?php $aux = $u->criacao; $aux1 = substr($aux, 2); echo $aux1;?></p></div>
													</li>
												<?php } ?>
											</ul>
											<div class = 'row'>
												<div>
													<a class = "btn btn-primary" id ="number_closed" data-value = "6">Ver mais incidentes</a>
												</div>
											</div>
									</div>
								</div >
							<div id="btn_div">
								
							</div>
					</div>
				</div>
				</div>
			</div>
		<script>
		$("#open_btn").on("click", function(){
			$("#closed_incidents").fadeOut("slow");
			$("#open_incidents").fadeIn("slow");
		});
		$("#closed_btn").on("click", function(){
			$("#open_incidents").fadeOut("slow");
			$("#closed_incidents").fadeIn("slow");

		});
		$("#number_open").on("click", function(ev){
			base_url = $("#base_url").attr("data-urlbase");
			number = $("#number_open").attr("data-value");
			$.getJSON(base_url+"incident/ver_mais_open/"+number, function(data)
			{
				$.each(data, function(index, item){
					console.log (item.titulo);
						if (item.status == 0)
							{
								var aux = 'Aguardando atendimento';
							} else 
							{
								var aux = 'Finalizado';
							}
							var aux2 = item.criacao.substr(0,2);
							var text = '<li id = "incident_list">'+
														'<div class ="row">'+
															'<div class ="col-md-10">'+
															'<div id = "upper_incident_layout"><h3>'+item.titulo+'</h3></div>'+
															'<div id = "botton_incident_layout">'+
																'<a class = "btn btn-success" href="'+base_url+'detail_page/view_detail/'+item.id+ '"> Ver Detalhes</a>'+
																 ' <form action="" onsubmit="return onSubmit()" id = "dashboard_form_layout" method ="POST">'+
																	'<button class = "btn btn-success" name="incident_id" value='+item.id+ ' type="submit" >Excluir </button>'+
																'</form>'+
																' <a class = "btn btn-success" href="'+base_url+'incident/editar/'+item.id+ '"> Editar</a>'+
																' <i id = "icon_flag_open" class="glyphicon glyphicon-flag"></i>'+
																'<p id = "status_layout">'+
																aux+
																'</p>'+
															'</div>'+
														'</div>'+
														'<div class = "col-md-2" id="right_incident_layout"><p id ="text_day">'+aux2+'</p><p id = "text_data"><?php $aux = $u->criacao; $aux1 = substr($aux, 2); echo $aux1;?></p></div>'+
													'</li>';
					$("#incident_ul_open").append(text);
				});
		$("#number_open").attr("data-value", parseInt(number)+6);
			});
			ev.preventDefault();
		});
		$("#number_closed").on("click", function(ev){
			base_url = $("#base_url").attr("data-urlbase");
			number = $("#number_closed").attr("data-value");
			$.getJSON(base_url+"incident/ver_mais_closed/"+number, function(data)
			{
				$.each(data, function(index, item){
					console.log (item.titulo);
						if (item.status == 0)
							{
								var aux = 'Aguardando atendimento';
							} else 
							{
								var aux = 'Finalizado';
							}
							var aux2 = item.criacao.substr(0,2);
							var text = '<li id = "incident_list">'+
														'<div class = "row">'+
															'<div class = "col-md-10">'+
															'<div id = "upper_incident_layout"><h3>'+item.titulo+'</h3></div>'+
															'<div id = "botton_incident_layout">'+
																'<a class = "btn btn-success" href="'+base_url+'detail_page/view_detail/'+item.id+ '"> Ver Detalhes</a>'+
																 ' <form action="" onsubmit="return onSubmit()" id = "dashboard_form_layout" method ="POST">'+
																	'<button class = "btn btn-success" name="incident_id" value='+item.id+ ' type="submit" >Excluir </button>'+
																'</form>'+
																' <a class = "btn btn-success" href="'+base_url+'incident/editar/'+item.id+ '"> Editar</a>'+
																' <i id = "icon_flag_closed" class="glyphicon glyphicon-flag"></i>'+
																'<p id = "status_layout">'+
																aux+
																'</p>'+
															'</div>'+
														'</div>'+
														'<div class = "col-md-2" id="right_incident_layout"><p id ="text_day">'+aux2+' </p><p id = "text_data"><?php $aux = $u->criacao; $aux1 = substr($aux, 2); echo $aux1;?></p></div>'+
													'</li>';
					$("#incident_ul_closed").append(text);
				});
		$("#number_closed").attr("data-value", parseInt(number)+6);
			});
			ev.preventDefault();
		});
		</script>
		</section>