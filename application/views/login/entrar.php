		<section>
			<div id ="container">
				<div class = "row">
					<div id ="login_div" class = "col-md-3">
						<form action="login/validarlogin" onsubmit="return onSubmit()" method ="POST">
							<p id= "text_login">Email</p>
							<input type="text" class ="input-sm" id = "input_email" name = "email" placeholder="Email"></input>
							<p id = "text_login">Senha</p>
							<input type="password" class = "input-sm" id ="input_password" name = "password" placeholder="Senha"></input>
							<div id="btn_submit">
								<button id ="button_submit" type="submit" class="btn btn-primary">Entrar</button>
							</div>
						</form>
					</div>
					<div id="error_message"></div>
				</div>
			</div>
		</section>
		<script type="text/javascript">
			$("INPUT").on("click", function(){
				$("#error_message").fadeOut("slow");
			});
		</script>
