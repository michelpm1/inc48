		<section>
			<div id ="container">
				<div class = "row">
					<div class ="col-md-3" id = "pag_initial_menu">
						<div id='background'>
						<?php $avatar = $avatar_array[0]->avatar; 
						$avatar_base_url = base_url()."static/avatar/";
						$aux = $avatar_base_url.$avatar;
						?>
						<img src="<?php echo $aux;?>" target="_blank" class="img-circle" id = "avatar">
						<p id="user_name"><?php echo $this->session->userdata('nome_usuario');?></p>
						</div>
						<div>
						<ul class ="list_nav">
							<li class="nav_links">
							<a href ="<?php echo base_url();?>dashboard_admin">
							<p class = "text_nav">Incidentes</p></a><div class = "incident_cont"><?php echo $count_incident[0]->incidents;?></div></li>
							<li class="nav_links">
							<a href ="<?php echo base_url();?>my_data_admin"> 
							<p class = "text_nav">Meus dados</p></a></li>
							<li class="nav_links">
							<a href ="<?php echo base_url();?>login/sair">
							<p class = "text_nav">Sair</p></a></li>
						</ul>
						<i id = "nav_icon1" class="glyphicon glyphicon-off"></i>
						<i id = "nav_icon2" class="glyphicon glyphicon-user"></i>
						<i id = "nav_icon3" class="glyphicon glyphicon-th-list"></i>
						</div>
						</div>
				</div>
				<div class = "row">
					<div class = "col-md-8">
					<h1 class = "incident_text_my_data">Meus Dados</h1>
					<div class ="mydata_form">
						<div style="position: relative;left: 23;top: 37;">
						</div>
							<form action="<?php echo base_url();?>my_data/edit_user" onsubmit="return Validate()" method ="POST">
							<p class = "my_data_text">Nome</p>
							<input type="text" class="form-control" style="width:54%;" name= "changed_user_name" value = "<?php echo $this->session->userdata('nome_usuario');?>"></input>
							<p class = "my_data_text">Email</p>
							<input type="text" class="form-control" style="width:54%;" name = "changed_user_email" id= "changed_user_email" value = "<?php echo $this->session->userdata('email_usuario');?>"></input>
							<p class = "my_data_text">Digite sua nova senha</p>
							<input type="password" class="form-control" style="width:54%;" name = "changed_password" id = "changed_password"></input>
							<p class = "my_data_text">Confirme sua nova senha</p>
							<input type="password" class="form-control" style="width:54%;" name = "confirm_password" id = "confirm_password"></input>
							<input id ="btn_mydata_submit_incident" type="submit" class="btn btn-primary" value = "salvar"></input>
							</form>
						</div>
				</div>
				</div>
			</div>
		</section>
		<script type="text/javascript">
		function Validate(){
			var aux ='';
			var aux_final='';
			var result =1;
			var checar = document.querySelector("#changed_user_email");
			console.log(checar);
			var test = checar.value.indexOf("@");
			if (test == -1){
				aux = 'Email inválido' + '\n';
				aux_final = 'Email inválido'+'\n';
				result =0;
			}
			var checar = document.querySelector("#changed_password");
			var passw=  /^[A-Za-z]\w{7,14}$/;
			if(!checar.value.match(passw)){
				aux_final = aux + 'Password inválido';
				result =0;
			}
			if (result == 0){
				alert(aux_final);
			}
			//var checar1 = document.querySelector("#changed_password");
			console.log(checar);
			var checar2 = document.querySelector("#confirm_password");
			console.log(checar2);
			if(checar.value != checar2.value){
				alert('senhas não coincidem');
			result = 0;
			}
			if (result == 0){
				return (false);
			}
		}
		</script>
