<?php

class Usuario_model extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function validar_usuario ($email, $password) {
		$this->db->where('email', $email);
		$this->db->where('senha', $password);
		$usuarios_result = $this->db->get('usuario');
		if (COUNT($usuarios_result -> result()) > 0)
		{
			return $usuarios_result -> result();
		}
		else{
			return array();
		}
	}

	function get_avatar ($id){
		$a = $this->db->query('select avatar from usuario where id = ?', array($id));
		return $a -> result();
	}

	function get_owner ($id){
		$a = $this->db->query('select name from usuario where id = ?', array($id));
		return $a -> result();
	}

	function change_name_email_password ($id, $changed_name, $changed_email, $changed_password){
		$edit_array = array(
		"name" => $changed_name,
		"email" => $changed_email,
		"senha" => $changed_password
		);
		$this->db->where('id', $id);
		$edit_result = $this->db->update('usuario', $edit_array); 
		return $edit_result;
	}
}