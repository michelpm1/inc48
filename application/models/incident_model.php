<?php

class Incident_model extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function create_incident($incident_radio, $subject_input, $description_input, $upload_file, $usuario_id) {
		$incident_array = array(
			"tipo" => $incident_radio,
			"titulo" => $subject_input,
			"descricao" => $description_input,
			"anexo" => $upload_file,
			"status" => 0,
			"criacao" => date('j M y'),
			"id_usuario" => $usuario_id
		);
		$verification = $this->db->insert('incidentes', $incident_array);
		return $verification;
	}
		function get_incident_open($usuario_id){
			$a = $this->db->query('select * from incidentes where id_usuario = ? and status = 0 limit 6', array($usuario_id));
			return $a ->result();
//			$verification = $this->db->get('incidentes');
//		return $verification;
		}
		function get_incident_closed($usuario_id){
			$a = $this->db->query('select * from incidentes where id_usuario = ? and status = 1 limit 6', array($usuario_id));
			return $a -> result();
//			$verification = $this->db->get('incidentes');
//		return $verification;
		}
			function get_incident_by_id($id){
			$a = $this->db->query('select * from incidentes where id = ?', array($id));
			return $a -> result();
}
		function delete_incident($incident_id){
			$delete_array = array(
			"id" => $incident_id
			);
			$this->db->delete('incidentes', $delete_array);
		}
		function edit_incident($incident_radio, $subject_input, $description_input, $id, $anexo){
			$edit_array = array(
			"tipo" => $incident_radio,
			"titulo" => $subject_input,
			"descricao" => $description_input,
			"anexo" => $anexo
			);
			$this->db->where('id', $id);
			$edit_result = $this->db->update('incidentes', $edit_array); 
			return $edit_result;
		}
		function ver_mais_incident_open($usuario_id, $number){
			$a = $this->db->query('select * from incidentes where id_usuario = ? and status = 0 limit ?, 6', array(intval($usuario_id),intval($number)));
			return $a -> result();
		}
		function ver_mais_incident_closed($usuario_id, $number){
			$a = $this->db->query('select * from incidentes where id_usuario = ? and status = 1 limit ?, 6', array(intval($usuario_id),intval($number)));
			return $a -> result();
		}
		function count_incident($id_usuario){
			$a = $this->db->query('select count(id_usuario) as incidents from incidentes where status = 0 and id_usuario = ?', array(intval($id_usuario)));
			return $a -> result();
		}
		function count_incident_admin($id_usuario){
			$a = $this->db->query('select count(id_usuario) as incidents from incidentes where status = 0');
			return $a -> result();
		}
		function get_all_incidents(){
			$a = $this->db->query('select * from incidentes where status = 0 limit 6');
			return $a ->result();
		}
		function ver_mais_incident_all($number){
			$a = $this->db->query('select * from incidentes where status = 0 limit ?, 6', array(intval($number)));
			return $a -> result();
		}
		function insert_answer($answer, $id){
			$edit_array = array(
			"resposta" => $answer,
			"status" => 1
			);
			$this->db->where('id', $id);
			$verification = $this->db->update('incidentes', $edit_array);
			return $verification;
		}
}