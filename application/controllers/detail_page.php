<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Detail_page extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('incident_model');
		$this->load->model('usuario_model');
	}
	public function view_detail($id)
	{
		if ($this->session->userdata('user_access') != 4){
			$aux = $this->incident_model->count_incident($this->session->userdata('usuario_id'));
			$avatar = $this->usuario_model->get_avatar($this->session->userdata('usuario_id'));
			$this->load->view('template/cabecalho');
			$b = $this->incident_model->get_incident_by_id($id);
			$this->load->view('incident/detail_page', array('count_incident' => $aux, 'incident_show_by_id' => $b, 'avatar_array' => $avatar));
			$this->load->view('template/rodape');
		} else{
			$aux = $this->incident_model->count_incident_admin($this->session->userdata('usuario_id'));
			$avatar = $this->usuario_model->get_avatar($this->session->userdata('usuario_id'));
			$b = $this->incident_model->get_incident_by_id($id);
			$incident_owner = $this->usuario_model->get_owner($b[0]->id_usuario);
			$this->load->view('template/cabecalho');
			$this->load->view('incident/detail_page_admin', array('incident_own' => $incident_owner, 'count_incident' => $aux, 'incident_show_by_id' => $b, 'avatar_array' => $avatar));
			$this->load->view('template/rodape');
		}
	}
}