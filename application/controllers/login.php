<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('usuario_model');
	}
	public function index()
	{
		$this->load->view('template/cabecalho');
		$this->load->view('login/entrar');
		$this->load->view('template/rodape');
	}
	public function listar()
	{
		$usuarios_result = $this->db->get('usuario');
		$test = $usuarios_result -> result();
		$this->load->view('template/cabecalho');
		$this->load->view('template/rodape');
//é possivel usar desta forma também para conectar ao database>>
//		$this->db->query('select * from usuario');
	}
	public function validarlogin()
	{
		if(COUNT($_POST)>0){
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$verification = $this->usuario_model->validar_usuario($email, $password);
			if (!empty($verification))
			{
				$this->session->set_userdata('nome_usuario', $verification[0]->name);
				$this->session->set_userdata('email_usuario', $verification[0]->email);
				$this->session->set_userdata('usuario_id', $verification[0]->id);
				$this->session->set_userdata('user_access', $verification[0]->access);
				if ($verification[0]->access != 4){
					redirect('dashboard');
				} else {
					redirect(base_url().'dashboard_admin');
				}
			}
			else
			{
				redirect(base_url());
			}
		}
	}
	public function sair()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}
}