<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Incident extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('incident_model');
		$this->load->model('usuario_model');
	}
	public function criar()
	{
		$aux = $this->incident_model->count_incident($this->session->userdata('usuario_id'));
		$avatar = $this->usuario_model->get_avatar($this->session->userdata('usuario_id'));
		if(COUNT($_POST)>0){
			$incident_radio = $this->input->post('incident_radio');
			$subject_input = $this->input->post('subject_input');
			$description_input = $this->input->post('description_input');
			$upload_file = $this->input->post('upload_file');
			$usuario_id = $this->session->userdata('usuario_id');
			$test = $this->grava_anexo();
			$anexo = $test['upload_data']['file_name']; //obs: matriz forma : Array ( [upload_data] => Array ( [file_name] =>
			$verification = $this->incident_model->create_incident($incident_radio, $subject_input, $description_input, $anexo, $usuario_id);
			if ($verification = 1)
			{
				$this->session->set_flashdata('db_message', 'Incidente criado com sucesso!');
			}
			else 
			{
				$this->session->set_flashdata('db_message', 'Ocorreu um erro na criação do incidente!');
			}
			redirect(base_url().'dashboard');
		}
		else
		{
		$this->load->view('template/cabecalho');
		$this->load->view('incident/incident_form',array('count_incident' => $aux, 'avatar_array' => $avatar));
		$this->load->view('template/rodape');
		}
	}
	public function grava_anexo()
		{
			$config['upload_path'] = '/var/www/incdesk/static/anexo';
			$config['allowed_types'] = 'gif|jpg|png|pdf';
			$config['max_size']	= '5000';
//			$config['max_width']  = '';
//			$config['max_height']  = '';

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('upload_file'))
			{
				$error = array('error' => $this->upload->display_errors());

				return $error;
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());

				return $data;
			}
		}
	public function editar($id)
	{
		$aux = $this->incident_model->count_incident($this->session->userdata('usuario_id'));
		$avatar = $this->usuario_model->get_avatar($this->session->userdata('usuario_id'));
		$this->load->view('template/cabecalho');
		$b = $this->incident_model->get_incident_by_id($id);
		$this->load->view('incident/editar_incident', array('incident_show' => $b, 'count_incident' => $aux, 'avatar_array' => $avatar));
		$this->load->view('template/rodape');
	}
	public function edit_database_incident()
	{
		if(COUNT($_POST)>0){
			$incident_radio = $this->input->post('incident_radio');
			$subject_input = $this->input->post('subject_input');
			$description_input = $this->input->post('description_input');
			$id = $this->input->post('id');
			$upload_file = $this->input->post('upload_file');
			$test = $this->grava_anexo();
			$anexo = $test['upload_data']['file_name']; //obs: matriz forma : Array ( [upload_data] => Array ( [file_name] =>
			$verification = $this->incident_model->edit_incident($incident_radio, $subject_input, $description_input, $id, $anexo);
			if ($verification == 1)
			{
				$this->session->set_flashdata('db_message', 'Incidente editado com sucesso!');
			}
			else 
			{
				$this->session->set_flashdata('db_message', 'Ocorreu um erro na edição do incidente!');
			}
			redirect(base_url().'dashboard');
		}
	}
	public function ver_mais_open($number)
		{
			$usuario_id = $this->session->userdata('usuario_id');
			$b = $this->incident_model->ver_mais_incident_open($usuario_id, $number);
			echo json_encode($b);
		}
	public function ver_mais_closed($number)
		{
			$usuario_id = $this->session->userdata('usuario_id');
			$b = $this->incident_model->ver_mais_incident_closed($usuario_id, $number);
			echo json_encode($b);
		}
	public function ver_mais_all($number)
		{
			$b = $this->incident_model->ver_mais_incident_all($number);
			echo json_encode($b);
		}
}