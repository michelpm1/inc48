<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Answer_incident extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('incident_model');
		$this->load->model('usuario_model');
	}
	public function reply_incident($id)
	{
		if ($this->session->userdata('user_access') != 4)
		{
			echo "You dont have permission!";
			redirect(base_url().'dashboard');
		} else {
			if(COUNT($_POST)>0){
				$answer = $this->input->post('answer_input');
				$aux = $this->incident_model->insert_answer($answer, $id);
				redirect(base_url().'dashboard_admin');
			} else{
				$aux = $this->incident_model->count_incident_admin($this->session->userdata('usuario_id'));
				$avatar = $this->usuario_model->get_avatar($this->session->userdata('usuario_id'));
				$b = $this->incident_model->get_incident_by_id($id);
				$this->load->view('template/cabecalho');
				$this->load->view('incident/answer_incident', array( 'count_incident' => $aux, 'incident_show_by_id' => $b, 'avatar_array' => $avatar));
				$this->load->view('template/rodape');
			}
		}
	}
}