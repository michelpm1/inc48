<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('incident_model');
		$this->load->model('usuario_model');
	}
	public function index()
	{
		$base_url = base_url();
		$dashboard = 'dashboard';
		$url_header = $base_url.$dashboard;
		$aux = $this->incident_model->count_incident($this->session->userdata('usuario_id'));
		$avatar = $this->usuario_model->get_avatar($this->session->userdata('usuario_id'));
		if(COUNT($_POST)>0)
		{
			$incident_id = $this->input->post('incident_id');
			$this->incident_model->delete_incident($incident_id);
			$this->load->view('template/cabecalho');
			$b = $this->incident_model->get_incident_open($this->session->userdata('usuario_id'));
			$c = $this->incident_model->get_incident_closed($this->session->userdata('usuario_id'));
			$this->load->view('dashboard', array('count_incident' => $aux, 'incident_show_open' => $b, 'avatar_array' => $avatar, 'incident_show_closed' => $c));
			$this->load->view('template/rodape');
			header("Location: $url_header");
		}
		else
		{
			$this->load->view('template/cabecalho');
			//$b = $this->incident_model->get_incident($this->session->userdata('usuario_id'));
			//$this->load->view('dashboard', array('incident_show' => $b));
			//$this->load->view('template/rodape');
			$b = $this->incident_model->get_incident_open($this->session->userdata('usuario_id'));
			$c = $this->incident_model->get_incident_closed($this->session->userdata('usuario_id'));
			$this->load->view('dashboard', array('count_incident' => $aux, 'incident_show_open' => $b, 'incident_show_closed' => $c, 'avatar_array' => $avatar));
			$this->load->view('template/rodape');
		}
	}
//	public function delete()
//	{
//		if(COUNT($_POST)>0)
//		{
//			$this->incident_model->delete_incident($_POST['incident_number']);
//			$this->load->view('template/cabecalho');
//			$b = $this->incident_model->get_incident($this->session->userdata('usuario_id'));
//			$this->load->view('dashboard', array('incident_show' => $b));
//			$this->load->view('template/rodape');
//		}
//	}
}

