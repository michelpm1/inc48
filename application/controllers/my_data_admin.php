<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class My_data_admin extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('incident_model');
		$this->load->model('usuario_model');
	}
	public function index(){
	if ($this->session->userdata('user_access') != 4){
		echo "You dont have permission!";
		redirect(base_url().'dashboard');
	}
		$aux = $this->incident_model->count_incident_admin($this->session->userdata('usuario_id'));
		$avatar = $this->usuario_model->get_avatar($this->session->userdata('usuario_id'));
		if(COUNT($_POST)>0)
		{
			$this->load->view('template/cabecalho');
			$this->load->view('my_data_admin',array('count_incident' => $aux, 'avatar_array' => $avatar));
			$this->load->view('template/rodape');
		}
		else
		{
			$this->load->view('template/cabecalho');
			$this->load->view('my_data_admin',array('count_incident' => $aux, 'avatar_array' => $avatar));
			$this->load->view('template/rodape');
		}
	}
	public function edit_user(){
		if(COUNT($_POST)>0){
			$changed_name = $this->input->post('changed_user_name');
			$changed_email = $this->input->post('changed_user_email');
			$changed_password = $this->input->post('changed_password');
			$id = $this->session->userdata('usuario_id');
			$this->usuario_model->change_name_email_password($id, $changed_name, $changed_email, $changed_password);
			$url_header = base_url()."login/sair";
			header("Location: $url_header");
		}
	}
}

